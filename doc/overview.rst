Overview
========
This little script performs peak finding using a Constant Fraction
Discriminator (CFD) algorithm.

See the `Wikipedia article
<https://en.wikipedia.org/wiki/Constant_fraction_discriminator>`_
for more
