from math import pi, sin
import numpy as np
from os.path import dirname, join
import pickle
import unittest

from peakfinder import (
    cfd, cursor_peaks, threshold_peaks, get_centroids
)

SINE_SIGNAL = np.array([sin(2 * pi * 500 * idx / 8000) for idx in range(64)])
SQUARE_SIGNAL = np.array([100, 100, 100, 0, 0, 0, 0] * 10)


class DigitizerProcessor_TestCase(unittest.TestCase):

    def setUp(self):
        gepicklet = join(dirname(__file__), "certified_peaks.pickle")
        with open(gepicklet, "rb") as fin:
            # SQS data proposal 900049 run 200 for threshold
            # SQS data proposal 900049 run 215 for cursor
            self.peaks = pickle.load(fin)

    def test_cfd(self):
        """Test Constant Fraction Discriminator"""
        signal = SINE_SIGNAL
        fraction = 0.5

        edges = list(cfd(signal, fraction))
        known_edges = [0, 8, 16, 24, 32, 40, 48, 56]
        self.assertSequenceEqual(edges, known_edges)

        signal = SQUARE_SIGNAL
        fraction = 0.1
        delay = 11
        edges = list(cfd(signal, fraction, delay))
        known_edges = [2, 5, 9, 12, 16, 19, 23, 26, 30, 33,
                       37, 40, 44, 47, 51, 54, 58, 62, 65]
        self.assertSequenceEqual(edges, known_edges)

    def test_threshold_peaks(self):
        """Test Threshold Peak Finding"""
        for _, test_data, expected in self.peaks['threshold']:
            edges = threshold_peaks(test_data, -100)
            np.testing.assert_array_equal(edges, expected)

    def test_cursor_peaks(self):
        """Test Cursor Peak Finding"""
        for _, test_data, expected in self.peaks['cursor']:
            edges = cursor_peaks(test_data, 3.5, 3.5)
            np.testing.assert_array_equal(edges, expected)

    def test_get_centroids(self):
        """Test Centroids"""
        edges = [2, 4, 6, 8]
        centroids = get_centroids(edges)
        self.assertSequenceEqual(centroids, [3, 7])

        edges = [2, 4, 6]
        centroids = get_centroids(edges)
        self.assertSequenceEqual(centroids, [3])

        edges = []
        centroids = get_centroids(edges)
        self.assertSequenceEqual([], centroids)


if __name__ == "__main__":
    unittest.main()
