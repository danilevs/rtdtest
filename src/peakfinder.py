import numpy as np
from scipy.ndimage.interpolation import shift


def timeit(func):
    """A timing decorator"""
    from time import perf_counter
    from functools import wraps

    @wraps(func)
    def wrapper(*args, **kwargs):
        t_start = perf_counter()
        res = func(*args, **kwargs)
        elapsed = perf_counter() - t_start
        print("{} took {}".format(func.__name__, elapsed))
        return res
    return wrapper


@timeit
def cfd(signal, fraction, delay=1, threshold=0, full_peaks=False):
    """Constant Fraction Discriminator

    :param np.array signal: the input from the digitizer
    :param float fraction: the fraction, from 0 to 1
    :param float delay: the delay of the CFD
    :param float threshold: the threshold at which the CFD triggers.
            In an ideal world, it would be zero, however the digitizer
            can operate at a level slightly above or below zero, and the
            noise can create false positives.
    :param bool full_peaks: return only pairs starting from a rising edge,
            such that we only have full peaks, and discard the first falling
            edge if the signal starts with one, and the last rising edge if
            the signal ends with one
    :return np.array edge_indices: the start and stop indices for each peak
    """

    # Scaled and delayed signal
    scaled = signal * fraction
    delayed = shift(scaled, -delay, mode="nearest")
    cfd_signal = signal - delayed

    # Edge detection
    edges_bool = cfd_signal > threshold
    edges_bool_2 = edges_bool[1:] != edges_bool[:-1]
    edge_indices = np.squeeze(np.where(edges_bool_2 == True))

    return edge_indices


@timeit
def cursor_peaks(signal, sigma_low=3, sigma_high=3,
                 distance=150, full_peaks=False):
    """Like on a scope, the region between low and high is the
    noise region, and anything sticking out of this region is a peak.

    The high and low limits are first defined by using the mean and
    adding/subtracting the standard deviation. The standard deviation
    can be further tuned by specifying sigmas.

    :param np.array signal: the input from the digitizer
    :param float sigma_low: sigma value for the lower range
    :param float signal_high: sigma value for the higher range
    :param bool full_peaks: return only pairs starting from a rising edge,
            such that we only have full peaks, and discard the first falling
            edge if the signal starts with one, and the last rising edge if
            the signal ends with one

    return np.array edge_indices: the start and stop indices for each peak
    """

    mean = np.mean(signal)
    deviation = signal.std()
    low = mean - deviation * sigma_low
    high = mean + deviation * sigma_high
    peak_data, = np.where((signal < low) | (signal > high))

    edge_indices = [peak_data[0]]
    for idx in range(1, len(peak_data)-1):
        if peak_data[idx + 1] - peak_data[idx] > distance:
            edge_indices.append(peak_data[idx])
            edge_indices.append(peak_data[idx + 1])
    edge_indices.append(peak_data[-1])

    return np.array(edge_indices)


@timeit
def threshold_peaks(signal, threshold=100, full_peaks=False):
    """Threshold-based peak detection

    This has shown to be more effective on shorter peaks in the signal,
    for instance, when only a single datum was sampled.

    :param np.array signal: the input from the digitizer
    :param int threshold: threshold value above which it's a peak
    :param bool full_peaks: return only pairs starting from a rising edge,
            such that we only have full peaks, and discard the first falling
            edge if the signal starts with one, and the last rising edge if
            the signal ends with one

    return np.array edge_indices: the start and stop indices for each peak
    """
    edge_indices, = np.where(signal > threshold) if threshold > 0 \
        else np.where(signal < threshold)

    return edge_indices


def get_centroids(edge_indices):
    """Given a list of edge_indices, get the centroids of each peak
    """
    centroids = []

    try:
        for idx in range(0, len(edge_indices), 2):
            centroid = int(edge_indices[idx] +
                           (edge_indices[idx + 1] - edge_indices[idx]) / 2)
            centroids.append(centroid)
    except IndexError:
        # Odd number of edges
        pass

    return centroids
